<?php
/**
 * Template Name: Home Page
 */
?>

<?php get_header(); ?>

            <!-- Main Section -->
            <section id="main">
                <!-- Slider -->
<div class="fullwidthbanner-container tp-banner-container">
    <div class="fullwidthbanner rslider tp-banner">
        <ul>
            <!-- THE FIRST SLIDE -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-thumb="<?php echo get_template_directory_uri(); ?>/website/img/0002.png" data-delay="10000"  data-saveperformance="on">

                <img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png"  alt="Secure GPS Tracking Platform" title="Secure GPS Tracking Platform" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/0002.png" data-bgposition="right top" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center bottom">

                <div class="tp-caption light_heavy_50 skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="30"
                     data-y="20"
                     data-speed="500"
                     data-start="2400"
                     data-easing="Power3.easeInOut"
                     data-splitin="chars"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">GPS Tracking Platform
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="110"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="117"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2850"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 8;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Motor bike GPS tracker" title="Motor bike GPS tracker" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="110"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Realtime Tracking
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="150"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="157"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3400"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 11;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Real time tracking" title="Real time tracking" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="150"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Theft alert
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="190"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="197"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 11;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Theft Alert" title="Theft Alert" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="190"
                     data-speed="300"
                     data-start="3800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Remote engine off
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="230"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="236"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4400"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Remote engine off" title="Remote engine off" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="230"
                     data-speed="300"
                     data-start="4300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Door Detection
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="270"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="276"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4900"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Door Detection" title="Door Detection" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="270"
                     data-speed="300"
                     data-start="4800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Voice Monitoring
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="310"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="316"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5500"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Voice Monitoring" title="Voice Monitoring" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="310"
                     data-speed="300"
                     data-start="5400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">SOS button
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="350"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="356"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="6000"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="SOS Button" title="SOS Button" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="350"
                     data-speed="300"
                     data-start="5900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">High sensitivity GPS chip
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="390"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="6400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="396"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="6500"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="High sensitivity GPS chip" title="High sensitivity GPS chip" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="390"
                     data-speed="300"
                     data-start="6400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Air-Condition On/Off
                </div>



                <div class="caption lft"
                     data-x="900"
                     data-y="0"
                     data-speed="300"
                     data-start="5200"
                     data-easing="easeOutExpo">
                    <img src="<?php echo get_template_directory_uri(); ?>/website/img/pricing/basic-gps-device-4000-taka.png" alt="basic-gps-device" title="basic-gps-device" height="240" width="225">
                </div>

            </li>

            <li data-transition="fade" data-slotamount="1" data-masterspeed="300">

               <img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png"  alt="laptopmockup_sliderdy" title="laptopmockup_sliderdy" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/0001.jpg" data-bgposition="right center" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="100" data-bgfitend="110" data-bgpositionend="left bottom">

                <div class="tp-caption white_heavy_60 skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                    data-x="0"
                    data-y="73"
                    data-speed="500"
                    data-start="2400"
                    data-easing="Power3.easeInOut"
                    data-splitin="chars"
                    data-splitout="none"
                    data-elementdelay="0.1"
                    data-endelementdelay="0.1"
                    data-endspeed="300"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">BD-Fleet Family
                </div>
          
                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="150"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="157"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2850"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 8;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Smallest Personal Tracker for Kids and Family" title="Smallest Personal Tracker for Kids and Family" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="150"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Smallest Personal Tracker for Kids and Family
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="190"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="197"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3400"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 11;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Track your Loved one any time" title="Track your Loved one any time" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="190"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Track your Loved one any time
                </div>
                
                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="230"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="236"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4000"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="SOS Alert and 2 Way Communication" title="SOS Alert and 2 Way Communication" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="230"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">SOS Alert and 2 Way Communication
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="270"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="276"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4600"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Secure GPS tracking Platform" title="Secure GPS tracking Platform" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="270"
                     data-speed="300"
                     data-start="4500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">GPS Tracking Platform
                </div>
                
                <div class="caption large_text sft"
                     data-x="900"
                     data-y="54"
                     data-speed="300"
                     data-start="800"
                     data-easing="easeOutExpo"  >24/7 SAFETY</div>
                <div class="caption medium_text sfl"
                     data-x="900"
                     data-y="92"
                     data-speed="300"
                     data-start="1100"
                     data-easing="easeOutExpo"  >FOR </div>
                <div class="caption large_text sfr"
                     data-x="950"
                     data-y="88"
                     data-speed="300"
                     data-start="1100"
                     data-easing="easeOutExpo"  ><span class="dblue">FAMILY</span></div>

                <div class="caption lfr"
                     data-x="850"
                     data-y="104"
                     data-speed="300"
                     data-start="1500"
                     data-easing="easeOutExpo">
                    <img src="<?php echo get_template_directory_uri(); ?>/website/img/smart-tracking-from-mobile-phone.png" alt="smart-tracking-from-mobile-phone" title="smart-tracking-from-mobile-phone" height="400" width="350">
                </div>

                <div class="caption lft"
                     data-x="660"
                     data-y="0"
                     data-speed="300"
                     data-start="5200"
                     data-easing="easeOutExpo">
                    <img src="<?php echo get_template_directory_uri(); ?>/website/img/pricing/gps-device-kids-family-member-tracking-3000-taka.png" alt="gps-device-kids-family-member-tracking" title="gps-device-kids-family-member-tracking" height="240" width="225">
                </div>

            </li>
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1000">

               <img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png"  alt="Fleet Management" title="Fleet Management" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/003.jpg" data-bgposition="right center" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="100" data-bgfitend="110" data-bgpositionend="left bottom">
               

               <div class="tp-caption customin fadeout rs-parallaxlevel-10"
                     data-x="690"
                     data-y="50"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2000"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 2;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="GPS tracker for Truck" title="GPS tracker for Truck" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/redbg_big.png" width="370px" height="100px">
                </div>

                <div class="tp-caption light_heavy_34 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="710"
                     data-y="83"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Fleet Management
                </div>


                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="150"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Service & Asset Management
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="150"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="156"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2850"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 8;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Workforce Management" title="Workforce Management" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>

                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="190"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Workforce Management
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="190"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="197"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3400"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 11;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Advanced trip Planning with SLA Sharing" title="Advanced trip Planning with SLA Sharing" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>


                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="230"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Advanced trip Planning with SLA Sharing
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="230"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="236"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4000"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Advance GeoFence with 24/7 Support" title="Advance GeoFence with 24/7 Support" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>


                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="270"
                     data-speed="300"
                     data-start="4500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Advance GeoFence with 24/7 Support
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="270"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="276"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4600"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Fuel Report" title="Fuel Report" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png"/>
                </div>

                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="310"
                     data-speed="300"
                     data-start="5100"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Fuel Report
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="310"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5100"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="316"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5200"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="GPS device with fuel report" title="GPS device with fuel report" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>

                <div class="caption lft"
                     data-x="30"
                     data-y="0"
                     data-speed="300"
                     data-start="5200"
                     data-easing="easeOutExpo">
                    <img src="<?php echo get_template_directory_uri(); ?>/website/img/pricing/gps-device-with-fuel-report-6300-taka.png" alt="gps-device-with-fuel-report" title="gps-device-with-fuel-report" height="240" width="225">
                </div>
            </li>

        </ul>
    </div>
</div>
<!-- Slider -->
                
                  <div class="container" style="margin-top: 40px;">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 text-center">
                                <h2 class="wow fadeIn"><?php the_field('products_title'); ?></h2>
                                <h3 class="wow fadeInRight"><?php the_field('products_title_tags'); ?></h3>
                  
                        </div>
                        <?php $price_post = get_field('price_post'); ?>
                        <div class="row">
                            <div class="padding-top40 text-center">
                            <?php
                                   // check if the repeater field has rows of data
                                   if( have_rows('price_posts') ):

                                             // loop through the rows of data
                                        while ( have_rows('price_posts') ) : the_row();
                                                                           
                                             $image = get_sub_field('price_post_image');
                                             $content = get_sub_field('price_post_content');
                                             $link = get_sub_field('price_post_link'); ?>
                                             
                                             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 post-item wow fadeInUp">
                                                  <div class="post-img">
                                                  <?php if( $link ): ?>
                                                       <a title="<?php echo $image['title']; ?>" href="<?php echo $link; ?>">
                                                            <img alt="<?php echo $image['title']; ?>" src="<?php echo $image['url']; ?>" title="<?php echo $image['title']; ?>">
                                                       </a>
                                                  <?php endif; ?>
                              
                                                  </div>
                                                  <div class="post-content blog-post-content">
                                                       <p>
                                                            <?php echo $content; ?>
                                                       </p>
                                                  </div>
                                             </div>

                                             <?php
                                        endwhile;

                                   endif;
                              ?>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Main Content -->
                                <!-- /Main Content -->
                <!-- Product Content -->
                <div class="product-lead bottom-pad margin-top100">
                    <div class="pattern-overlay">
                        <div class="container">
                            <div class="row">
                              <?php
                                   // vars
                                   $desc = get_field('poducts_descriptions');	

                                   if( $desc ): ?>
                                        <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft">
                                        <div class="app-service padding-bottom50">
                                             <h2 class="light"><?php echo $desc['products_description_title']; ?></h2>
                                             <p><?php echo $desc['products_description_body']; ?></p>
                                        </div>
                                   </div>
                                   
                                   <div class="col-md-6 col-sm-12 col-xs-12 text-center wow fadeInRight">
                                        <div class="app-service" style="padding-top: 100px;">
                                             <img class="app-img img-responsive" alt="<?php echo $desc['products_description_video']['title']; ?>" src="<?php echo $desc['products_description_video']['url']; ?>" title="<?php echo $desc['products_description_video']['title']; ?>">
                                        </div>
                                        <!-- <div class="app-service padding-bottom50"> -->
                                             <!-- <video playsinline autoplay loop="" muted="" id="myVideo1"  width="100%">
                                                  <source src="<?php /* echo $desc['products_description_video']['url']; */ ?>" type="video/mp4">
                                             </video> -->
                                        <!-- </div> -->
                                   </div>

                              <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="main-content">
                <?php

                    if( have_rows('products_display') ): 

                         while( have_rows('products_display') ): the_row(); ?>

                    <div class="container">
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <h2 class="wow fadeIn"><?php echo get_sub_field('products_display_title'); ?></h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <?php
                              if( have_rows('product_display_items') ):
                                   while ( have_rows('product_display_items') ) : the_row();
                                                                      
                                        $image = get_sub_field('display_items_image');
                                        $name = get_sub_field('display_item_name');?>
                                                
                                        <div class="product_display_items col-lg-4 col-md-4 col-sm-12 wow fadeIn">
                                                
                                             <div class="content-box big ch-item bottom-pad-small">

                                                  <div class="content-box-info">
                                                       <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>" title="<?php echo $image['title']; ?>"/>
                                                       <p>
                                                       <?php echo $name; ?>
                                                       </p>
                                                  </div>

                                             </div>
                                             
                                             <!-- <div class="img_plus col-md-1" text-align="center"><img src="/website/img/plus.png" id="image_plus" alt="Cloud software for GPS tracking" height="50px" width="50px"/></div> -->

                                        </div>

                                        <?php
                                   endwhile;

                              endif;
                         ?>
                        </div>
                    </div>
               <?php
                    endwhile;
                endif; ?>
                    
                </div>

                <!-- /Product Content --> 
            
                <!-- Latest Posts --> 
                  
                <!-- /Latest Posts -->

              
                <!-- /Random Facts -->
                
 
            </section>
            <!-- /Main Section -->

<?php get_footer(); ?>

