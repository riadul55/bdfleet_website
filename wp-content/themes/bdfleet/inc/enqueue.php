<?php

function bdfleet_css_js(){
   // Google Fonts

   // Library CSS
   wp_enqueue_style('bootstrap', get_template_directory_uri().'/website/css/bootstrap.css', array(), '1.0.0', 'all');
   wp_enqueue_style('bootstrap-theme', get_template_directory_uri().'/website/css/bootstrap-theme.css', array(), '1.0.0', 'all');
   wp_enqueue_style('font-awesome', get_template_directory_uri().'/website/css/fonts/font-awesome/css/font-awesome.css', array(), '', 'all');
   wp_enqueue_style('animations', get_template_directory_uri().'/website/css/animations.css', array(), '1.0.0', 'all');
   wp_enqueue_style('superfish', get_template_directory_uri().'/website/css/superfish.css', array(), '1.0.0', 'screen');
   wp_enqueue_style('settings', get_template_directory_uri().'/website/css/revolution-slider/css/settings.css', array(), '1.0.0', 'all');
   wp_enqueue_style('extralayers', get_template_directory_uri().'/website/css/revolution-slider/css/extralayers.css', array(), '1.0.0', 'all');
   wp_enqueue_style('prettyPhoto', get_template_directory_uri().'/website/css/prettyPhoto.css', array(), '1.0.0', 'all');
   // Theme CSS
   wp_enqueue_style('style', get_template_directory_uri().'/website/css/style.css', array(), '1.0.0', 'all');
   wp_enqueue_style('green', get_template_directory_uri().'/website/css/colors/green.css', array(), '1.0.0', 'all');
   wp_enqueue_style('theme-responsive', get_template_directory_uri().'/website/css/theme-responsive.css', array(), '1.0.0', 'all');
   wp_enqueue_style('switcher', get_template_directory_uri().'/website/css/switcher.css', array(), '1.0.0', 'all');
   wp_enqueue_style('spectrum', get_template_directory_uri().'/website/css/spectrum.css', array(), '1.0.0', 'all');
   wp_enqueue_style('myStyle', get_template_directory_uri().'/website/css/myStyle.css', array(), '1.0.0', 'all');




   // The Scripts -->
   wp_enqueue_script('jquery-main', get_template_directory_uri().'/website/js/jquery.min.js', array(), '1.0.0', true);
   wp_enqueue_script('jquery-migrate', get_template_directory_uri().'/website/js/jquery-migrate-1.0.0.js', array(), '1.0.0', true);
   wp_enqueue_script('jquery-ui', get_template_directory_uri().'/website/js/jquery-ui.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('bootstrap', get_template_directory_uri().'/website/js/bootstrap.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('themepunch.plugins', get_template_directory_uri().'/website/js/revolution-slider/js/jquery.themepunch.plugins.min.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('themepunch.revolution', get_template_directory_uri().'/website/js/revolution-slider/js/jquery.themepunch.revolution.min.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('parallax', get_template_directory_uri().'/website/js/jquery.parallax.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('wait', get_template_directory_uri().'/website/js/jquery.wait.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('fappear', get_template_directory_uri().'/website/js/fappear.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('modernizr', get_template_directory_uri().'/website/js/modernizr-2.6.2.min.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('bxslider', get_template_directory_uri().'/website/js/jquery.bxslider.min.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('prettyPhoto', get_template_directory_uri().'/website/js/jquery.prettyPhoto.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('superfish', get_template_directory_uri().'/website/js/superfish.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('tweetMachine', get_template_directory_uri().'/website/js/tweetMachine.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('tytabs', get_template_directory_uri().'/website/js/tytabs.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('gmap', get_template_directory_uri().'/website/js/jquery.gmap.min.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('sticky', get_template_directory_uri().'/website/js/jquery.sticky.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('countTo', get_template_directory_uri().'/website/js/jquery.countTo.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('jflickrfeed', get_template_directory_uri().'/website/js/jflickrfeed.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('imagesloaded', get_template_directory_uri().'/website/js/imagesloaded.pkgd.min.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('waypoints', get_template_directory_uri().'/website/js/waypoints.min.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('wow', get_template_directory_uri().'/website/js/wow.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('fitvids', get_template_directory_uri().'/website/js/jquery.fitvids.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('spectrum', get_template_directory_uri().'/website/js/spectrum.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('switcher', get_template_directory_uri().'/website/js/switcher.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('product', get_template_directory_uri().'/website/js/product.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('simpleGallery', get_template_directory_uri().'/website/js/simpleGallery/jquery.simpleGallery.js', array('jquery'), '1.0.0', true);
   wp_enqueue_script('simpleLens', get_template_directory_uri().'/website/js/simpleGallery/jquery.simpleLens.js', array('jquery'), '1.0.1', true);
   wp_enqueue_script('custom', get_template_directory_uri().'/website/js/custom.js', array('jquery'), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'bdfleet_css_js');
