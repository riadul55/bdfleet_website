<!DOCTYPE html>

<html>


<!-- Mirrored from www.bdfleet.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Mar 2019 08:45:17 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <title title="<?php the_field('site_title'); ?>"><?php the_field('site_title'); ?></title>
   
    <meta name="description" content="<?php the_field('site_meta_description'); ?>" />
<meta name="keywords" content="<?php the_field('site_meta_keywords'); ?>" />

    <meta name="author" content="<?php the_field('site_meta_author'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/LOGO.png">

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]>
    <link rel="stylesheet" href="css/ie.css">
    <![endif]-->


<?php wp_head(); ?>

</head>
<body class="home">
        <div class="page-mask">
            <div class="page-loader">
                <div class="spinner"></div>
                Loading...
            </div>
        </div>
        <!-- Wrap -->
        <div class="wrap">
            <header id="header">
    <!-- Header Top Bar -->

    <div class="top-bar">
        <div class="slidedown collapse">
            <div class="container">
                <div class="pull-left">
                    <ul class="social pull-left">
                        <li class="facebook"><a title="facebook" href="https://www.facebook.com/bdfleetgps/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        
                        
                        <li class="linkedin"><a title="linkedIn" href="https://www.linkedin.com/company/bdfleet" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        
                    </ul>
                </div>
                <div class="phone-login pull-right">
                    <i class="fa fa-phone"></i> Call Us : +8802 51057312 or +880 1317263465
                </div>
            </div>
        </div>
    </div>

    <!-- /Header Top Bar -->
    <!-- Main Header -->
    <div class="main-header">
        <div class="container">

            <!-- TopNav -->
            <div class="topnav navbar-header">
                <a title="Top Nav" class="navbar-toggle down-button" data-toggle="collapse" data-target=".slidedown">
                    <i class="fa fa-angle-down icon-current"></i>
                </a> 
            </div>
            <!-- /TopNav-->
            <!-- Logo -->
            <div class="logo pull-left">
                <h1 style="margin:0px !important;">
                    <a title="Logo" href="http://bdfleet.com">
                         <img class="logo-color" style="margin-bottom: 0px;" src="<?php echo get_template_directory_uri(); ?>/website/img/yuma.png" alt="BDFLEET" title="BDFLEET" width="50" height="50">
                         <h1 style="display: inline-block; font-family: sans-serif; text-shadow: 2px 2px 2px #A9A9A9; margin-left: -10px;"><span style="color: #ffa600;">BD</span><span style="color: #A9A9A9;">Fleet</span></h1>
                    </a>
                </h1>
		<span style="margin-left:70px; transform:translateY(-10px);display:inline-block;color: #0d6b4f;font-weight: bold;">GPS Tracking Platform, Bangladesh</span>
            </div>
            <!-- /Logo -->
            <!-- Mobile Menu -->
         <!--   <div class="mobile navbar-header">
                <a class="navbar-toggle abc" data-toggle="collapse" href="#navbar-collapse-id">
                <i class="fa fa-bars "></i>
                </a> 
            </div> -->
            <!-- /Mobile Menu -->
            <!-- Menu Start -->
            <!-- <nav class="collapse navbar-collapse menu" aria-expanded="true">
            </nav>-->

                 <?php
                    wp_nav_menu(array(
                        'theme_location' => 'main_menu',
                        'container' => 'div',
                        'container_class' => 'collapse navbar-collapse menu navbar-right',
			'container_id' => 'navbar-collapse-id',
                        'menu_class' => 'nav navbar-nav sf-menu',
                        'depth' => '2',
                        'fallback_cb'=> 'WP_Bootstrap_Navwalker::fallback',
                        'walker'=> new WP_Bootstrap_Navwalker()
                    ));
                ?>

            <!-- /Menu -->
        </div>
    </div>
    <!-- /Main Header -->
</header>

