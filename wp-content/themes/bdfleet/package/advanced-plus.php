<?php
/**
 * Template Name: Package Advanced-plus Page
 */
?>


<?php get_header(); ?>

    <section id="main">

        <!-- Main Content -->
        <div class="content margin-top60 margin-bottom60">

            <div class="container">


			<?php $price_post = get_field('advance_plus_images'); ?>
                    <div class="row">
                           <?php
                                   // check if the repeater field has rows of data
                                   if( have_rows('advance_plus_images') ):
					$i=0;
                                             // loop through the rows of data
                                        while ( have_rows('advance_plus_images') ) : the_row();
                                             $i++;
                                             $image = get_sub_field('product_image'); ?>
                                             
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 post-item wow fadeInDown" data-wow-delay="<?php echo $i ?>s">
                                                  <div class="post-img" style="text-align:center;">

                                                            <img style="width:80%" alt="<?php echo $image['title']; ?>" src="<?php echo $image['url']; ?>" title="<?php echo $image['title']; ?>">                              
                                                  </div>
                                             </div>

                                             <?php
                                        endwhile;

                                   endif;
                              ?>       
                    </div>




                <div class="row">
                    <h2 style="margin-bottom: 50px; color: #FFAE17;">ADVANCED Plus Package</h2>
                    <h2 style="width: 100%;">Features</h2>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <table class="">
                            <thead>
                            <tr>
                                <th width="30%"></th>
                                <th width="70%"></th>
                            </tr>
                            </thead>
                            <tbody >
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li5.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    ACC detection for ignition status
                                    
                                </td>

                            </tr>
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/engine_off.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Remote engine/oil cut
                                    
                                </td>

                            </tr>

                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/geofense.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Unlimited geo-fence
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/push_notification.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Real time push notification
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Built_In_Battery_Backup.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Built in battery backup
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Vibration_Alert.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Vibration alert
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/battery_power_cut.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Battery disconnection/Low Alert
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li6.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Voice Monitoring
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/map_customization.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Location customization
                                </td>

                            </tr>

                            <tr>
                                <td class="td-images">
                                    <a class="product-image" href="shop-product-view.html" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li11.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Air condition on/off detection
                                </td>

                            </tr>

                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <table class="">
                            <thead>
                            <tr>
                                <th width="30%"></th>
                                <th width="70%"></th>
                            </tr>
                            </thead>
                            <tbody >
                           <tr>
                                <td class="td-images" >
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/route_update.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Route update
                         
                                </td>

                            </tr>
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li15.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Real time tracking
                                </td>
                                 
                                <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/googlemap.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Google map and trafiq update
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/route_playback.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Route history playback
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/over_speed_alert.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   Over speed alert
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Warranty.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   3 Years Device Warranty
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image"  title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Various%20anti-theft-alarms.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   Various anti-theft alarms
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/sos_button.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    SOS Button
                                </td>

                            </tr>

                            <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li10.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Door Detection
                                </td>

                            </tr>
                            
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/fuel_report.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Fuel report
                                </td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row margin-top60">
                    <h2>Specifications</h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">Hardware </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Voice monitor range</td>
                                    <td>≤5meters</td>
                                </tr>
                                <tr>
                                    <td>Battery</td>
                                    <td>450mAh/3.7V Li-Poiymer battery</td>
                                </tr>
                                <tr>
                                    <td>Working Voltage/current</td>
                                    <td>9-36VDC/45mA(12VDC), 28mA(24VDC)</td>
                                </tr>
                                <tr>
                                    <td>Operating Temperature</td>
                                    <td>-20℃～ 70℃</td>
                                </tr>
                                <tr>
                                    <td>Dimension</td>
                                    <td>80(L) ×55.8.0 (W) ×23.4 (H) mm</td>
                                </tr>
                                <tr>
                                    <td>Weight</td>
                                    <td>95g </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">GSM </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>GSM frequency</td>
                                    <td>850/900/1800/1900 MHz</td>
                                </tr>
                                <tr>
                                    <td>GPRS</td>
                                    <td>Class 12, TCP/IP</td>
                                </tr>
                                <tr>
                                    <td>Memory</td>
                                    <td>32+32Mb</td>
                                </tr>
                                <tr>
                                    <td>Phase error</td>
                                    <td>RMSPE<5,PPE<20</td>
                                </tr>
                                <tr>
                                    <td>Max output</td>
                                    <td>GSM850/GSM900:33±3dBm
                                        GSM1800/GSM1900:30±3Bm</td>
                                </tr>
                                <tr>
                                    <td>Max frequency error</td>
                                    <td>±0.1ppm</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">GPS </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>GPS chipset</td>
                                    <td>MTK high sensitivity GPS chip</td>
                                </tr>
                                <tr>
                                    <td>Frequency</td>
                                    <td>L1,1575.42MHz C/A code</td>
                                </tr>
                                <tr>
                                    <td>GPS channel</td>
                                    <td>Built-in, 25*25*4mm</td>
                                </tr>
                                <tr>
                                    <td>Location accuracy</td>
                                    <td><10 meters</td>
                                </tr>
                                <tr>
                                    <td>Tracking sensitivity</td>
                                    <td>-162dBm</td>
                                </tr>
                                <tr>
                                    <td>Acquisition sensitivity</td>
                                    <td>-148dBm</td>
                                </tr>
                                <tr>
                                    <td>TTFF (open sky)</td>
                                    <td>Avg. hot start≤1sec
                                        Avg. cold start≤35sec</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>


<?php get_footer(); ?>

