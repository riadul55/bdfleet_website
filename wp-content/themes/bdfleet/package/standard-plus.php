<?php
/**
 * Template Name: Package Standard-plus Page
 */
?>

<?php get_header(); ?>

    <section id="main">

        <!-- Main Content -->
        <div class="content margin-top60 margin-bottom60">

            <div class="container">

                <div class="row">
                    <h2 style="margin-bottom: 50px; color: #FFAE17;">STANDARD Plus Package</h2>
                    <h2 style="width: 100%;">Features</h2>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <table class="">
                            <thead>
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li5.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    ACC detection for ignition status
                                    
                                </td>

                            </tr>
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/engine_off.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Remote engine/oil cut
                                    
                                </td>

                            </tr>

                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/geofense.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Unlimited geo-fence
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image"  title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/push_notification.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Real time push notification
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="Features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Built_In_Battery_Backup.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Built in battery backup
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="Features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Vibration_Alert.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Vibration alert
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image"  title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/battery_power_cut.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Battery disconnection/Low Alert
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image"  title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li6.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Voice Monitoring
                                </td>

                            </tr>



                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <table class="">
                            <thead>
                            <tr>
                                <th width="30%"></th>
                                <th width="70%"></th>
                            </tr>
                            </thead>
                            <tbody >
                           <tr>
                                <td class="td-images" >
                                    <a class="product-image"  title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/route_update.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Route update
                         
                                </td>

                            </tr>
                            <tr>
                                <td class="td-images">
                                    <a class="product-image"  title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li15.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Real time tracking
                                </td>
                                 
                                <tr>
                                <td class="td-images">
                                    <a class="product-image"  title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/googlemap.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Google map and trafiq update
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image"  title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/route_playback.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Route history playback
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="Features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/over_speed_alert.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   Over speed alert
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="Features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Warranty.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   3 Years Device Warranty
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="Features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Various%20anti-theft-alarms.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   Various anti-theft alarms
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="Features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/sos_button.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    SOS Button
                                </td>

                            </tr>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row margin-top60">
                    <h2>Specifications</h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">Hardware </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Built-in Battery</td>
                                    <td>A 400mAh/3.7Vindustrial lithium polymer battery</td>
                                </tr>
                                <tr>
                                    <td>Dimension</td>
                                    <td>98(L) *52.0 (W) *15.0 (H) mm</td>
                                </tr>
                                <tr>
                                    <td>Weight</td>
                                    <td>80G</td>
                                </tr>
                                <tr>
                                    <td>Operating Temperature</td>
                                    <td>-20℃~70℃ </td>
                                </tr>
                                <tr>
                                    <td>LED Indicator</td>
                                    <td>GPS(blue), GSM(green), Power(red) </td>
                                </tr>

                                <tr>
                                    <td>Antenna</td>
                                    <td>Built-in GPS ceramic antenna; GSM quad-band antenna </td>
                                </tr>

                                <tr>
                                    <td>GPS Chip</td>
                                    <td>MTK high sensitivity GPS chip </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">GPS </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Location Accuracy</td>
                                    <td><10 meters</td>
                                </tr>
                                <tr>
                                    <td>Tracking Sensitivity</td>
                                    <td>– 165dBm</td>
                                </tr>
                                <tr>
                                    <td>Frequency</td>
                                    <td>L1,1575.42MHz C/A code</td>
                                </tr>
                                <tr>
                                    <td>Location time</td>
                                    <td>Hot start: ≤ 2sec(open sky) && Cold start: ≤35sec(open sky)</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>

<?php get_footer(); ?>