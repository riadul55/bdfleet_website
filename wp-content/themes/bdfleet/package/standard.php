<?php
/**
 * Template Name: Package Standard Page
 */
?>
<?php get_header(); ?>

    <section id="main">

        <!-- Main Content -->
        <div class="content margin-top60 margin-bottom60">

            <div class="container">
			
			
			<?php $price_post = get_field('standard_product_image'); ?>
                    <div class="row">
                           <?php
                                   // check if the repeater field has rows of data
                                   if( have_rows('standard_product_image') ):
					$i=0;
                                             // loop through the rows of data
                                        while ( have_rows('standard_product_image') ) : the_row();
                                             $i++;                              
                                             $image = get_sub_field('product_image'); ?>
                                             
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 post-item wow fadeInDown" data-wow-delay="<?php echo $i ?>s">
                                                  <div class="post-img" style="text-align:center;">

                                                            <img style="width:80%" alt="<?php echo $image['title']; ?>" src="<?php echo $image['url']; ?>" title="<?php echo $image['title']; ?>">                              
                                                  </div>
                                             </div>

                                             <?php
                                        endwhile;

                                   endif;
                              ?>       
                    </div>



                <div class="row">
                    <h2 style="margin-bottom: 50px; color: #FFAE17;">STANDARD Package</h2>
                    <h2 style="width: 100%;">Features</h2>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <table class="">
                            <thead>
                            <tr>
                                <th width="30%"></th>
                                <th width="70%"></th>
                            </tr>
                            </thead>
                            <tbody >
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li5.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    ACC detection for ignition status
                                    
                                </td>

                            </tr>
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/engine_off.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Remote engine/oil cut
                                    
                                </td>

                            </tr>

                            <tr>
                                <td class="td-images">
                                    <a class="product-image" href="shop-product-view.html" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/geofense.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Unlimited geo-fence
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="shop-product-view.html" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/push_notification.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Real time push notification
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="shop-product-view.html" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Built_In_Battery_Backup.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Built in battery backup
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="shop-product-view.html" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Vibration_Alert.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Vibration alert
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="shop-product-view.html" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/battery_power_cut.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Battery disconnection/Low Alert
                                </td>

                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <table class="">
                            <thead>
                            <tr>
                                <th width="30%"></th>
                                <th width="70%"></th>
                            </tr>
                            </thead>
                            <tbody >
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/route_update.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Route update
                         
                                </td>

                            </tr>
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li15.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Real time tracking
                                </td>
                                 
                                <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/googlemap.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Google map and trafiq update
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/route_playback.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Route history playback
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/over_speed_alert.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   Over speed alert
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Warranty.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   1 Years Device Warranty
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Various%20anti-theft-alarms.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   Various anti-theft alarms
                                </td>

                            </tr>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row margin-top60">
                    <h2>Specifications</h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">Hardware </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Battery</td>
                                    <td>120mAh</td>
                                </tr>
                                <tr>
                                    <td>Chipset</td>
                                    <td>MTK MT6261M</td>
                                </tr>
                                <tr>
                                    <td>GPS Chipset</td>
                                    <td>UBX G7020</td>
                                </tr>
                                <tr>
                                    <td>GPS Signal</td>
                                    <td>L1,1575.42MHz C/A </td>
                                </tr>
                                <tr>
                                    <td>Minimum Current</td>
                                    <td>0.0316A </td>
                                </tr>

                                <tr>
                                    <td>Average Current</td>
                                    <td>0.0560A </td>
                                </tr>

                                <tr>
                                    <td>Maximum  Current</td>
                                    <td>0.1865A </td>
                                </tr>

                                <tr>
                                    <td>Working Temperature</td>
                                    <td>-20 ~ 70℃ </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">GPS </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>GSM+GPRS+GPS</td>
                                    <td>GPS Location</td>
                                </tr>
                                <tr>
                                    <td>GPS Signal</td>
                                    <td>L1,1575.42MHz C/A</td>
                                </tr>
                                <tr>
                                    <td>GPS Sensitivity</td>
                                    <td>Tracking :-165dBm,Acquisition :-148dBm</td>
                                </tr>
                                <tr>
                                    <td>AGPS</td>
                                    <td>Location in 26seconds</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>

<?php get_footer(); ?>