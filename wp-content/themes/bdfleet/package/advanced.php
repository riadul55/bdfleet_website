<?php
/**
 * Template Name: Package Advanced Page
 */
?>


<?php get_header(); ?>

    <section id="main">

        <!-- Main Content -->
        <div class="content margin-top60 margin-bottom60">

            <div class="container">

                <div class="row">
                    <h2 style="margin-bottom: 50px; color: #FFAE17;">ADVANCED Package</h2>
                    <h2 style="width: 100%;">Features</h2>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <table class="">
                            <thead>
                            <tr>
                                <th width="30%"></th>
                                <th width="70%"></th>
                            </tr>
                            </thead>
                            <tbody >
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li5.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    ACC detection for ignition status
                                    
                                </td>

                            </tr>
                            <tr>
                                <td class="td-images" >
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/engine_off.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Remote engine/oil cut
                                    
                                </td>

                            </tr>

                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/geofense.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Unlimited geo-fence
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/push_notification.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Real time push notification
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Built_In_Battery_Backup.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Built in battery backup
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Vibration_Alert.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Vibration alert
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/battery_power_cut.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Battery disconnection/Low Alert
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li6.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Voice Monitoring
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/map_customization.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Location customization
                                </td>

                            </tr>


                            </tr>

                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <table class="">
                            <thead>
                            <tr>
                                <th width="30%"></th>
                                <th width="70%"></th>
                            </tr>
                            </thead>
                            <tbody >
                              <tr>
                                <td class="td-images" >
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/route_update.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Route update
                         
                                </td>

                            </tr>
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li15.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Real time tracking
                                </td>
                                 
                                <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/googlemap.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Google map and trafiq update
                                </td>

                            </tr>
                            
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/route_playback.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Route history playback
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/over_speed_alert.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   Over speed alert
                                </td>

                            </tr>
                            
                             <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Warranty.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   3 Years Device Warranty
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features" >
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Various%20anti-theft-alarms.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                   Various anti-theft alarms
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td-images">
                                    <a class="product-image" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/sos_button.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    SOS Button
                                </td>

                            </tr>

                            <tr>
                                <td class="td-images">
                                    <a class="product-image" href="#" title="features">
                                        <img width="70" height="70" title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li10.png">
                                    </a>
                                </td>
                                <td class="td-name">
                                    Door Detection
                                </td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row margin-top60">
                    <h2>Specifications</h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">Hardware </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Power Supply</td>
                                    <td>10.6V – 36V / 1.5A</td>
                                </tr>
                                <tr>
                                    <td>Battery</td>
                                    <td>500mAh</td>
                                </tr>
                                <tr>
                                    <td>Dimension</td>
                                    <td>65 x 61 x 26mm</td>
                                </tr>
                                <tr>
                                    <td>Weight</td>
                                    <td>90g</td>
                                </tr>
                                <tr>
                                    <td>Operating temperature</td>
                                    <td>-20° to 55° C</td>
                                </tr>
                                <tr>
                                    <td>LED</td>
                                    <td>2 LED lights to show GPS/GSM status</td>
                                </tr>
                                <tr>
                                    <td>Flash Memory</td>
                                    <td>4MB</td>
                                </tr>
                                <tr>
                                    <td>Button</td>
                                    <td>One SOS Button && 3 digital inputs (2 negative and 1 positive triggering)</td>
                                </tr>
                                <tr>
                                    <td>Flash Memory</td>
                                    <td>4MB</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="40%">GPS </th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Frequency</td>
                                    <td>Quad Band GSM 850/900/1800/1900Mhz</td>
                                </tr>
                                <tr>
                                    <td>GPS Module</td>
                                    <td>latest GPS SIRF-Star IV chipset</td>
                                </tr>
                                <tr>
                                    <td>GPS Sensitivity</td>
                                    <td>-163Db</td>
                                </tr>
                                <tr>
                                    <td>GPS Frequency</td>
                                    <td>L1, 1575.42 MHz</td>
                                </tr>
                                <tr>
                                    <td>Position Accuracy</td>
                                    <td>10 meters, 2D RMS</td>
                                </tr>
                                <tr>
                                    <td>Velocity Accuracy</td>
                                    <td>0.1 m/s</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>


<?php get_footer(); ?>

