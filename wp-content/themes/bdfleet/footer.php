
            <!-- Footer -->
		
    <div id="ttt-footer">
        <div class="bg-wfgrey odd-dark">
            <footer class="container ttt-footer">
                <div class="ttt-bigfooter">
                    <div class="colgroup noborder nogap ttt-social">
                        <div class="row">
                            <div class="col first last col-xs-6 col-sm-6 col-md-6 ttt-social-media">
                                <span class="h5 title-primary">Follow</span>
                                <div id="social-media-icons">
                                    <a target="_blank" data-placement="bottom" data-toggle="tooltip" title="Follow us on Facebook" href="https://www.facebook.com/bdfleetgps/" data-original-title="Follow us on Facebook"><i class="fa fa-facebook"></i></a>
                                    <a target="_blank" data-placement="bottom" data-toggle="tooltip" title="Follow us on Twitter" href="" data-original-title="Follow us on Twitter"><i class="fa fa-twitter"></i></a>
                                    <a target="_blank" data-placement="bottom" data-toggle="tooltip" title="Follow us on LinkedIn" href="https://www.linkedin.com/company/yuma-technology-ltd/about/" data-original-title="Follow us on LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ttt-bigfooter ttt-bigfooter-nav">
                    <div class="colgroup noborder nogap margin-top-bot">
                        <div class="row">
                            <div class="col first col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="h5 title-primary">Products</div>
                                <ul class=" simplelist">
                                    <li><a href="https://bdfleet.com/" title="GPS Tracking" data-text="GPS Tracking">GPS Tracking</a></li>
                                    <li><a href="https://cloud.bdfleet.com/" title="Fleet management" data-text="Fleet management">Fleet management</a></li>
                                    <li><a href="https://account.bdfleet.com/" title="Cloud Accounting Software" data-text="Cloud Accounting Software">Cloud Accounting Software</a></li>
                                    <li><a href="https://bdfleet.com/contact-us/" title="Get a demo">Get a demo</a></li>
                                </ul>
                            </div>
                            <div class="col col-2nd-child col-xs-6 col-sm-6 col-md-9 col-lg-9 pull-right">
                                <div class="colgroup noborder nogap">
                                    <div class="row">
                                        <div class="col first col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <div class="h5 title-primary">Technology</div>
                                            <ul class=" simplelist">
                                                <li><a href="https://cloud.bdfleet.com/" title="BDFLEET" data-text="BDFLEET">BDfleet</a></li>
                                            </ul>
                                        </div>
                                        <div class="col col-2nd-child col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <div class="h5 title-primary">Buy</div>
                                            <ul class=" simplelist">
                                                <li><a href="https://bdfleet.com/contact-us/" title="How to buy">How to buy</a></li>
                                                <li><a href="https://bdfleet.com/contact-us/" title="Contact sales">Contact sales</a></li>
                                            </ul>
                                        </div>
                                        <div class="col col-3rd-child col-xs-6 col-sm-6 col-md-3 col-lg-3 no-padding-top">
                                            <div class="h5 title-primary footer-col-headline-not-first">Partners</div>
                                            <ul class=" simplelist">
                                                <li><a href="https://www.yuma-technology.co.uk/" title="Yuma Technology UK" data-text="Yuma Technology UK">Yuma Technology UK</a></li>
                                                <li><a href="https://www.tracksolid.com/" title="Track Solid" data-text="Track Solid">Track Solid</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <div class="bg-wfgrey odd-dark">
            <footer class="container ttt-footer">
                
                    <nav>
                            <ul id="ttt-home" class="footer ttt-navigation">
                                <li class="first">
                                    <a href="https://cloud.bdfleet.com/policy" target="_blank" rel="nofollow" title="Privacy policy" data-text="Privacy policy">
                                        <span>Privacy policy</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://cloud.bdfleet.com/policy" rel="nofollow" title="Terms &amp; Conditions" data-text="Terms &amp; Conditions">
                                        <span>Terms &amp; Conditions</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/company/yuma-technology-ltd/jobs/" title="Jobs" data-text="Jobs">
                                        <span>Jobs</span>
                                    </a>
                                </li>
                                <li class="last">
                                    <a href="https://bdfleet.com/" target="_blank" title="bdfleet.com" data-text="bdfleet.com">
                                        <span>bdfleet.com</span>
                                    </a>
                                </li>
                            </ul>

                            <address>Copyright &copy; 2005 - 
                                    <script type="text/javascript">
                                        document.write(new Date().getFullYear());
                                      </script>
					BD Fleet. All rights reserved.</address>
                        </nav>
            </footer>
        </div>
    </div>


<!-- Scroll To Top -->
<a title="scroll top" href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
        </div>

<?php wp_footer(); ?>

</body>


<!-- Mirrored from www.bdfleet.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Mar 2019 08:45:41 GMT -->
</html>
