<?php
/**
 * Template Name: Price Advanced Page
 */
?>

<?php get_header(); ?>

    <section id="main">

        <!-- choose-us -->
        <div class="choose-us container" style="margin-top: 30px">
            <div class="row" style="margin-bottom: 50px;">
                <div class="col-lg-12">
                    <div class="text-center">
                        <h2 class="wow fadeIn"><?php the_field('price_advance_header'); ?></h2>
                       
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h3 class="text-left">Features</h3>
                    <!-- Accordin -->
                    <div class="accordionMod panel-group">
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li3.png" height="40px" width="40px" style="margin-right: 10px"> Secure Platform</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Our platform certified by Mcafee secure certification </li>
                                    <li><i class="fa fa-check"></i> Our platform is hosted in our UK Base Datacenter in a secure facility</li>
                                    <li><i class="fa fa-check"></i> Our monitoring system always scan the platform for any issues</li>
                                  
                                  
                                </ul>
                            </section>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li4.png" height="40px" width="40px" style="margin-right: 15px"> Live Tracking</h4>
                            <section class="accordion-inner panel-body">
                                <ul> <li><i class="fa fa-check"></i> 24/7 Tacking platform</li>
                                    <li><i class="fa fa-check"></i> One dashboard for all your vehicles</li>
                                    <li><i class="fa fa-check"></i> Our intelligent monitoring system tracks GPS Devices and alert us if there is any problem</li>
                                    <li><i class="fa fa-check"></i> If Need, we can monitor your vehicle when you are in holiday. - Call us for arrangement</li>

                                </ul>
                            </section>
                        </div>
                         <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/engine_off.png" height="40px" width="40px" style="margin-right: 15px"> Secure Remote Engine/Oil Cut<br>
                                <a title="price-advanced" href="#" style="margin-left: 55px"><span style="font-style: italic; color: #a0a0a0; font-size: x-small">Phone SMS also From Web with Additional SMS authorization</span></a>
                            </h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> 2 Layer of Security when you want to turn off vehicle from web or android mobile -Call us to discuss </li>
                                    <li><i class="fa fa-check"></i> We will give you FREE SMS to turn off Engine or Oil if you dont have internet in your phone.</li>
                                    <li><i class="fa fa-check"></i> You can let us know when you are in holiday, we can track your vehicle 24/7(optional service)</li>
                                  
                                </ul>
                            </section>
                        </div>
                        
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/fuel_report.png" height="40px" width="40px" style="margin-right: 15px"> Fuel report <br>
                                
                                <a title="price-advanced" href="#" style="margin-left: 55px"><span style="font-style: italic; color: #a0a0a0; font-size: x-small">Accuracy of fuel report depends fuel device we install</span></a>
                            </h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Fuel consumption report </li>
                                    <li><i class="fa fa-check"></i> Alert when fuel level drop suddenly (Fuel theft)</li>
                                  
                                </ul>
                            </section>
                        </div>

                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li10.png" height="40px" width="40px" style="margin-right: 15px"> Door Detection</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                   <li><i class="fa fa-check"></i> Get alert when someone open door (any door).</li>
                                    <li><i class="fa fa-check"></i> You can let us know when you are in holiday  we can track your car 24/7(Optional Service)</li>
                                    <li><i class="fa fa-check"></i> You can customize "Alert time" depends on your criteria</li>
                                   
                                </ul>
                            </section>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/voice_call.png" height="40px" width="40px" style="margin-right: 15px"> Voice call</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                 <li><i class="fa fa-check"></i> Dial-in to your GPS Devices at any time.</li>
                                    <li><i class="fa fa-check"></i> BDFLEET ® only provides this service, does not take responsibilities regards to privacy policy of customer vehicle </li>
                                
                                </ul>
                            </section>
                        </div>
                        
                        
                        
                        
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/sos_button.png" height="40px" width="40px" style="margin-right: 15px"> SOS button</h4>
                           
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    
                                </ul>
                            </section>
                        </div>

                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li7.png" height="40px" width="40px" style="margin-right: 15px"> Location Customization</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> You can set multiple places in the map, we will notify you when your vehicle is nearby to those places (Comming Soon)</li>
                                    <li><i class="fa fa-check"></i> Share your location with another user with estimated time of arrival (Comming Soon) </li>
                                    <li><i class="fa fa-check"></i> Sent driving route to any user from your location to his location and monitor his location update in real time (Comming Soon)</li>
                             
                                </ul>
                            </section>
                        </div>

                       <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/geofense.png" height="40px" width="40px" style="margin-right: 15px"> Unlimited Geofence </h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Create unlimited geofence and get alert when your vehicle is In and Out of those area</li>
                                  
                                </ul>
                            </section>
                        </div>

                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li2.png" height="40px" width="40px" style="margin-right: 15px"> Air-condition On/Off Detection</h4>
                            <a title="price-advanced" href="#" style="margin-left: 55px"><span style="font-style: italic; color: #a0a0a0; font-size: x-small">Depends on packages</span></a>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                  <li><i class="fa fa-check"></i> Get alert when someone turn on Air-condition</li>
                                    <li><i class="fa fa-check"></i> Call us to discuss the price</li>
                                    
                                </ul>
                            </section>
                        </div>
                        
                        
                          
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/googlemap.png" height="40px" width="40px" style="margin-right: 15px"> Google Map</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Use Google map, GPS and LBS combination to track your vehicle  </li>
                                    <li><i class="fa fa-check"></i> Live Traffic update </li>
                                     <li><i class="fa fa-check"></i> Google satellite  view </li>
                                    
                                 
                                </ul>
                            </section>
                        </div>

                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Built_In_Battery_Backup.png" height="40px" width="40px" style="margin-right: 15px"> Built in battery backup </h4>
                            <section class="accordion-inner panel-body">
                               
                            </section>
                        </div>
                        
                        <div class="accordion-item">
                            <section class="accordion-inner panel-body">
                               
                            </section>
                        </div>
                        
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/battery_power_cut.png" height="40px" width="40px" style="margin-right: 15px"> 	Battery disconnection/Low Alert </h4>
                            <section class="accordion-inner panel-body">
                               
                            </section>
                        </div>
                        
                        
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Various%20anti-theft-alarms.png" height="40px" width="40px" style="margin-right: 15px">Various anti-theft alarms </h4>
                            <section class="accordion-inner panel-body">
                               
                            </section>
                        </div>
                        
                        
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/over_speed_alert.png" height="40px" width="40px" style="margin-right: 15px"> Over speed alert </h4>
                            <section class="accordion-inner panel-body">
                               
                            </section>
                        </div>
                        
                         <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Vibration_Alert.png" height="40px" width="40px" style="margin-right: 15px"> Vibration alert </h4>
                            <section class="accordion-inner panel-body">
                               
                            </section>
                        </div>
                        
                        
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/push_notification.png" height="40px" width="40px" style="margin-right: 15px"> Real time push notification </h4>
                            <section class="accordion-inner panel-body">
                               
                            </section>
                        </div>
                       
                        
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Warranty.png" height="40px" width="40px" style="margin-right: 15px"> Long Warrenty </h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Our devices come with 1 to 5 Year warranty </li>
                                    <li><i class="fa fa-check"></i> Please call us to  discuss your options</li>
                                 
                                </ul>
                            </section>
                        </div>
                    </div>
                    <!-- /Accordin -->
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <img title="" style="margin-top: 50px" alt="<?php echo get_field('price_advance_image')['alt']; ?>" src="<?php echo get_field('price_advance_image')['url']; ?>">
                </div>
            </div>
        </div>
        <!-- /choose-us -->

    </section>

<?php get_footer(); ?>