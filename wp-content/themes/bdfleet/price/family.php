<?php
/**
 * Template Name: Price Family Page
 */
?>

<?php get_header(); ?>
    <section id="main">

        <!-- choose-us -->
        <div class="choose-us container" style="margin-top: 30px">
            <div class="row" style="margin-bottom: 50px;">
                <div class="col-lg-12">
                    <div class="text-center">
                        <h2 class="wow fadeIn"><?php the_field('price_family_header'); ?></h2>
                      
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h3 class="text-left">Features</h3>
                    <!-- Accordin -->
                    <div class="accordionMod panel-group">
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li3.png" height="40px" width="40px" style="margin-right: 10px"> Secure Platform</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Our platform certified by Mcafee secure certification </li>
                                    <li><i class="fa fa-check"></i> Our platform is hosted in our UK Base Datacenter in a secure facility</li>
                                    <li><i class="fa fa-check"></i> Our monitoring system always scan the platform for any issues</li>
                                  
                                </ul>
                            </section>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li4.png" height="40px" width="40px" style="margin-right: 15px"> Live Tracking</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> 24/7 Tacking Platform</li>
                                    <li><i class="fa fa-check"></i> Watch your loved ones from anywhere</li>
                                    
                                </ul>
                            </section>
                        </div>
                       
                        
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/voice_call.png" height="40px" width="40px" style="margin-right: 15px"> 2way voice call</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> call and speak with your kids or loved one any time</li>
                                    <li><i class="fa fa-check"></i> your kids can speak with you just by pressing one single button </li>
                                
                                </ul>
                            </section>
                        </div>

                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/sos_button.png" height="40px" width="40px" style="margin-right: 15px"> SOS Button</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> At time of needs, your kids or loved one can contact with you just pressing one button, no need to dial number</li>
                                    
                                </ul>
                            </section>
                        </div>

                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/geofense.png" height="40px" width="40px" style="margin-right: 15px"> Unlimited Geofence </h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Get alert when your kids reach school or when they are in a bus</li>
                                  
                                </ul>
                            </section>
                        </div>
                         
                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/googlemap.png" height="40px" width="40px" style="margin-right: 15px"> Google Map</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Use Google map, GPS and LBS combination to stay in touch   </li>
                                    
                                 
                                </ul>
                            </section>
                        </div>
                        

                        <div class="accordion-item">
                            <h4 class="accordion-toggle"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/Warranty.png" height="40px" width="40px" style="margin-right: 15px"> Long Warrenty (Optional)</h4>
                            <section class="accordion-inner panel-body">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i> Our devices come with 1 to 5 Year warranty </li>
                                    <li><i class="fa fa-check"></i> Please call us to  discuss your options</li>
                                 
                                </ul>
                            </section>
                        </div>
                    </div>
                    <!-- /Accordin -->
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <img title="" style="margin-top: 50px" alt="<?php echo get_field('price_family_image')['alt']; ?>" src="<?php echo get_field('price_family_image')['url']; ?>">
                </div>
            </div>
        </div>
        <!-- /choose-us -->

    </section>
    <?php get_footer(); ?>
