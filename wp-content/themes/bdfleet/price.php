<?php
/**
 * Template Name: Price Page
 */
?>

<?php get_header(); ?>

    <section id="main">

        <!-- Slider -->
<div class="fullwidthbanner-container tp-banner-container">
    <div class="fullwidthbanner rslider tp-banner">
        <ul>
            <!-- THE FIRST SLIDE -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-thumb="<?php echo get_template_directory_uri(); ?>/website/img/0002.png" data-delay="10000"  data-saveperformance="on">

                <img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png"  alt="Secure GPS Tracking Platform" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/0002.png" data-bgposition="right top" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center bottom">

                <div class="tp-caption light_heavy_50 skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="30"
                     data-y="20"
                     data-speed="500"
                     data-start="2400"
                     data-easing="Power3.easeInOut"
                     data-splitin="chars"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">Secure Platform
                </div>
                <div class="tp-caption customin fadeout rs-parallaxlevel-10"
                     data-x="10"
                     data-y="10"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2700"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 2;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Car GPS tracker" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/redbg_big.png" width="400px" height="90px">
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="110"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="117"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2850"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 8;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Motor bike GPS tracker" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="110"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Realtime Tracking
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="150"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="157"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3400"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 11;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Real time tracking" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="150"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Theft alert
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="190"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="197"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 11;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Theft Alert" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="190"
                     data-speed="300"
                     data-start="3800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Remote engine off
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="230"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="236"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4400"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Remote engine off" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="230"
                     data-speed="300"
                     data-start="4300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Door Detection
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="270"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="276"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4900"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Door Detection" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="270"
                     data-speed="300"
                     data-start="4800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Voice Monitoring
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="310"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="316"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5500"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Voice Monitoring" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="310"
                     data-speed="300"
                     data-start="5400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">SOS button
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="350"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="356"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="6000"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="SOS Button" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="350"
                     data-speed="300"
                     data-start="5900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">High sensitivity GPS chip
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="390"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="6400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="396"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="6500"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="High sensitivity GPS chip" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="390"
                     data-speed="300"
                     data-start="6400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Air-Condition On/Off
                </div>



                <div class="caption lft"
                     data-x="900"
                     data-y="0"
                     data-speed="300"
                     data-start="5200"
                     data-easing="easeOutExpo">
                    <img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/pricing/basic-gps-device-4000-taka.png" alt="basic-gps-device" height="240" width="225">
                </div>

            </li>

            <li data-transition="fade" data-slotamount="1" data-masterspeed="300">

               <img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png"  alt="laptopmockup_sliderdy" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/0001.jpg" data-bgposition="right center" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="100" data-bgfitend="110" data-bgpositionend="left bottom">

                <div class="tp-caption white_heavy_60 skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                    data-x="0"
                    data-y="73"
                    data-speed="500"
                    data-start="2400"
                    data-easing="Power3.easeInOut"
                    data-splitin="chars"
                    data-splitout="none"
                    data-elementdelay="0.1"
                    data-endelementdelay="0.1"
                    data-endspeed="300"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">BD-Fleet Family
                </div>
          
                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="150"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="157"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2850"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 8;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Smallest Personal Tracker for Kids and Family" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="150"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Smallest Personal Tracker for Kids and Family
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="190"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="197"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3400"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 11;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Track your Loved one any time" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="190"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Track your Loved one any time
                </div>
                
                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="230"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="236"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4000"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="SOS Alert and 2 Way Communication" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="230"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">SOS Alert and 2 Way Communication
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="40"
                     data-y="270"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>
                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="45"
                     data-y="276"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4600"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Secure GPS tracking Platform" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>
                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="70"
                     data-y="270"
                     data-speed="300"
                     data-start="4500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Secure Platform
                </div>
                
                <div class="caption large_text sft"
                     data-x="900"
                     data-y="54"
                     data-speed="300"
                     data-start="800"
                     data-easing="easeOutExpo"  >24/7 SAFETY</div>
                <div class="caption medium_text sfl"
                     data-x="900"
                     data-y="92"
                     data-speed="300"
                     data-start="1100"
                     data-easing="easeOutExpo"  >FOR </div>
                <div class="caption large_text sfr"
                     data-x="950"
                     data-y="88"
                     data-speed="300"
                     data-start="1100"
                     data-easing="easeOutExpo"  ><span class="dblue">FAMILY</span></div>

                <div class="caption lfr"
                     data-x="850"
                     data-y="104"
                     data-speed="300"
                     data-start="1500"
                     data-easing="easeOutExpo">
                    <img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/smart-tracking-from-mobile-phone.png" alt="smart-tracking-from-mobile-phone" height="400" width="350">
                </div>

                <div class="caption lft"
                     data-x="660"
                     data-y="0"
                     data-speed="300"
                     data-start="5200"
                     data-easing="easeOutExpo">
                    <img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/pricing/gps-device-kids-family-member-tracking-3000-taka.png" alt="gps-device-kids-family-member-tracking" height="240" width="225">
                </div>

            </li>
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1000">

               <img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png"  alt="Fleet Management" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/003.jpg" data-bgposition="right center" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="100" data-bgfitend="110" data-bgpositionend="left bottom">
               

               <div class="tp-caption customin fadeout rs-parallaxlevel-10"
                     data-x="690"
                     data-y="50"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2000"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 2;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="GPS tracker for Truck" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/redbg_big.png" width="370px" height="100px">
                </div>

                <div class="tp-caption light_heavy_34 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="710"
                     data-y="83"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Fleet Management
                </div>


                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="150"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Service & Asset Management
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="150"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="156"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="2850"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 8;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Workforce Management" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>

                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="190"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Workforce Management
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="190"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3300"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="197"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3400"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 11;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Advanced trip Planning with SLA Sharing" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>


                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="230"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Advanced trip Planning with SLA Sharing
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="230"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="3900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="236"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4000"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Advance GeoFence with 24/7 Support" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>


                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="270"
                     data-speed="300"
                     data-start="4500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Advance GeoFence with 24/7 Support
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="270"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="276"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="4600"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="Fuel Report" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>

                <div class="tp-caption black_bold_bg_20 sfr fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="830"
                     data-y="310"
                     data-speed="300"
                     data-start="5100"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">Fuel Report
                </div>

                <div class="tp-caption greenbox30 customin fadeout tp-resizeme rs-parallaxlevel-10"
                     data-x="800"
                     data-y="310"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5100"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                </div>

                <div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
                     data-x="811"
                     data-y="316"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="5200"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 14;"><img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/dummy.png" alt="GPS device with fuel report" data-ww="17" data-hh="17" data-lazyload="<?php echo get_template_directory_uri(); ?>/website/img/check.png">
                </div>

                <div class="caption lft"
                     data-x="30"
                     data-y="0"
                     data-speed="300"
                     data-start="5200"
                     data-easing="easeOutExpo">
                    <img title="" src="<?php echo get_template_directory_uri(); ?>/website/img/pricing/gps-device-with-fuel-report-6300-taka.png" alt="gps-device-with-fuel-report" height="240" width="225">
                </div>
            </li>

        </ul>
    </div>
</div>
<!-- Slider -->

        <!-- Main Content -->
        <div class="content margin-bottom60 margin-top60">
            <div class="container">
                <div class="row" style="margin-bottom: 50px;">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <h1 class="wow fadeIn"><?php the_field('price_header'); ?></h1>
                            <h3 class="wow fadeInRight"><?php the_field('price_tags'); ?></h3>
                        </div>
                    </div>
                </div>

                <!-- 4 Column Pricing Table -->
                <div class="row">
                    <div class="col-sm-1 col-md-1"></div>
                    <div class="col-sm-5 col-md-5">
                        <div class="pricing_plan bottom-pad-small wow fadeInUp">
<!--                            <h3>Lite<strong>Plan</strong> <small>this is where you start</small></h3>-->
                            <a title="price-standard" href="#"><img title="" alt="<?php echo get_field('price_car_image')['alt']; ?>" src="<?php echo get_field('price_car_image')['url']; ?>"></a>
                            <div class="the_price">
<!--                                <span>?</span>350<small>/month</small>-->
                                <span>Facilities</span>
                            </div>
                            <div class="the_offerings">

                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li3.png" height="40px" width="40px" style="margin-right: 15px"> Secure Platform </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li4.png" height="40px" width="40px" style="margin-right: 15px"> Live Tracking  </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li5.png" height="40px" width="40px" style="margin-right: 15px"> Secure Remote Engine/Oil Cut<br>
                                    <a title="price-standard" href="#" style="margin-left: 55px"><span style="font-style: italic; color: #a0a0a0; font-size: x-small">Phone SMS also From Web with Additional SMS authorization</span></a>
                                </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li10.png" height="40px" width="40px" style="margin-right: 15px"> Door Detection<br>                               

                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li6.png" height="40px" width="40px" style="margin-right: 15px"> Voice Monitoring </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li7.png" height="40px" width="40px" style="margin-right: 15px"> Location Customization </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li8.png" height="40px" width="40px" style="margin-right: 15px"> Unlimited Geo-Fence </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li2.png" height="40px" width="40px" style="margin-right: 15px"> Air-condition On/Off Detection(Optional)</p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li9.png" height="40px" width="40px" style="margin-right: 15px"> Long Warranty (Optional) </p>

                            </div>
                            <a title="price-standard" href="https://bdfleet.com/price-standard/" class="btn btn-color">More...</a>
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-5">
                        <div class="pricing_plan bottom-pad-small wow fadeInUp">
<!--                            <h3>Medium<strong>Plan</strong> <small>this is what you should choose</small></h3>-->
                            <a title="price-advanced" href="#"><img title="" alt="<?php echo get_field('price_track_image')['alt']; ?>" src="<?php echo get_field('price_track_image')['url']; ?>"></a>
                            <div class="the_price">
<!--                                <small>From</small><span>?</span>450<small>/month</small>-->
                                <span>Facilities</span>
                            </div>
                            <div class="the_offerings">

                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li3.png" height="40px" width="40px" style="margin-right: 15px"> Secure Platform </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li4.png" height="40px" width="40px" style="margin-right: 15px"> Live Tracking  </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li5.png" height="40px" width="40px" style="margin-right: 15px"> Secure Remote Engine/Oil Cut<br>
                                    <a title="price-advanced" href="#" style="margin-left: 55px"><span style="font-style: italic; color: #a0a0a0; font-size: x-small"> Phone SMS also From Web with Additional SMS authorization</span></a>
                                </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li1.png" height="40px" width="40px" style="margin-right: 15px"> Fuel Report<br>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li10.png" height="40px" width="40px" style="margin-right: 15px"> Door Detection<br>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li7.png" height="40px" width="40px" style="margin-right: 15px"> Advanced location Sharing
                                <a title="price-advanced" href="#" style="margin-left: 55px"><span style="font-style: italic; color: #a0a0a0; font-size: x-small"><br>**for Business customer only**</span></a>
                                </p>
                               <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li12.png" height="40px" width="40px" style="margin-right: 15px"> Advanced Geo-Fence
                               <a title="price-advanced" href="#" style="margin-left: 55px"><span style="font-style: italic; color: #a0a0a0; font-size: x-small"><br>**for Business customer only**</span></a>
                               
                                </p>
                              
                      
                             
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li9.png" height="40px" width="40px" style="margin-right: 15px"> Long Warranty (Optional) </p>

                            </div>
                            <a title="price-advanced" href="https://bdfleet.com/price-advanced/" class="btn btn-color">More...</a>
                        </div>
                    </div>
                    <div class="col-sm-1 col-md-1"></div>
                </div>

                <div class="row">
                    <div class="col-sm-1 col-md-1"></div>
                    <div class="col-sm-5 col-md-5">
                        <div class="pricing_plan bottom-pad-small wow fadeInUp">
                            <!--                            <h3>Pro<strong>Plan</strong> <small>this is what you really need</small></h3>-->
                            <a title="price-bike" href="#"><img title="" alt="<?php echo get_field('price_bike_image')['alt']; ?>" src="<?php echo get_field('price_bike_image')['url']; ?>"></a>
                            <div class="the_price">
                                <span>Facilities</span>
<!--                                <span>?</span>259<small>/month</small>-->
                            </div>
                            <div class="the_offerings">

                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li3.png" height="40px" width="40px" style="margin-right: 15px"> Secure Platform </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li4.png" height="40px" width="40px" style="margin-right: 15px"> Live Tracking  </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li5.png" height="40px" width="40px" style="margin-right: 15px"> Secure Remote Engine/Oil Cut<br>
                                   
                                </p>
                                                            
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li8.png" height="40px" width="40px" style="margin-right: 15px"> Unlimited Geo-Fence </p>
                              
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li7.png" height="40px" width="40px" style="margin-right: 15px"> Location Customization </p>
                               
                          
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li9.png" height="40px" width="40px" style="margin-right: 15px"> Long Warranty (Optional) </p>

                            </div>
                            <a title="price-bike" href="https://bdfleet.com/price-bike/" class="btn btn-color">More...</a>
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-5">
                        <div class="pricing_plan wow fadeInUp">
                            <!--                            <h3>Pro<strong>Plan</strong> <small>this is what you really need</small></h3>-->
                            <a title="price-family" href="#"><img title="" alt="<?php echo get_field('price_family_image')['alt']; ?>" src="<?php echo get_field('price_family_image')['url']; ?>"></a>
                            <div class="the_price">
                                <span>Facilities</span>
<!--                                <span>?</span>249<small>/month</small>-->
                            </div>
                            <div class="the_offerings">

                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li3.png" height="40px" width="40px" style="margin-right: 15px"> Secure Platform </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li11.png" height="40px" width="40px" style="margin-right: 15px"> Waterproof smallest GPS tracking device </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li15.png" height="40px" width="40px" style="margin-right: 15px"> Real-time Tracking<br>
                                   
                                </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li13.png" height="40px" width="40px" style="margin-right: 15px"> SOS Alarm </p>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/li6.png" height="40px" width="40px" style="margin-right: 15px"> Voice Monitoring<br>
                                <p><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/website/img/voice_call.png" height="40px" width="40px" style="margin-right: 15px"> Two-way Voice Calling</p>

                            </div>
                            <a title="price-family" href="https://bdfleet.com/price-family/" class="btn btn-color">More...</a>
                        </div>
                    </div>
                    <div class="col-sm-1 col-md-1"></div>
                </div>
                <!-- /4 Column Pricing Table -->
            </div>
        </div>
        <!-- /Main Content -->

    </section>

    <?php get_footer(); ?>