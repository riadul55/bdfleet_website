<?php
/**
 * Template Name: Contact Us Page
 */
?>

<?php get_header(); ?>

    <section id="main">
        <div class="content margin-top60 margin-bottom60">
            <div class="container">
                <div class="row">
                    <div class="white-wrapper nopadding">
                        <!-- Star -->
                        <div class="clearfix"></div>
                        <div class="row padding-top margin-top">
                            <!-- Contact Details -->
                            <div class="contact-details">
                                <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                                    <div class="text-center">
                                        <div class="wow swing">
                                            <div class="contact-icon">
                                                <a title="marker" href="#" class=""> <i class="fa fa-map-marker fa-3x"></i> </a>
                                            </div>
                                            <!-- end dm-icon-effect-1 -->
                                            <p>House #631, Road 10, Avenue #3<br>
                                                Mirpur DOHS<br>
                                                Dhaka, Bangladesh

                                            </p>
                                        </div>
                                        <!-- end service-icon -->
                                    </div>
                                    <!-- end miniboxes -->
                                </div>
                                <!-- /col-lg-4 -->
                                <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                                    <div class="text-center">
                                        <div class="wow swing">
                                            <div class="contact-icon">
                                                <a title="phone" href="#" class=""> <i class="fa fa-phone fa-3x"></i> </a>
                                            </div>
                                            <p><strong>Phone:</strong> +8802 51057312<br>
                                                <strong>Mobile:</strong> 01317263465
                                            </p>
                                        </div>
                                        <!-- /service-icon -->
                                    </div>
                                    <!-- /miniboxes -->
                                </div>
                                <!--  /col-lg-4 -->
                                <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                                    <div class="text-center">
                                        <div class="wow swing">
                                            <div class="contact-icon">
                                                <a title="message" href="#" class=""> <i class="fa fa-envelope-o fa-3x"></i> </a>
                                            </div>
                                            <p><strong>Email:</strong> sales@bdfleet.com<br>
                                            </p>
                                        </div>
                                        <!--  /service-icon -->
                                    </div>
                                    <!-- /miniboxes -->
                                </div>
                                <!-- /col-lg-4 -->
                            </div>
                            <!-- Contact Details -->
                        </div>
                        <!-- /margin-top --><br><br>
                    </div>
                </div>
                <!-- Google Map -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 align="center">Our Location</h2>
                        <div id="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3649.459569055984!2d90.36540661429866!3d23.8378092913499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c1ba655aee57%3A0x5d262685342b959!2sYuma+Technology!5e0!3m2!1sen!2sbd!4v1552808420912" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- /Google Map -->
                <!-- Star -->
                <div class="star">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="star-divider">
                                <div class="star-divider-icon">
                                    <i class=" fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Star -->
                <div class="row">
                    <!-- /Contact Form -->
                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                        <div class="address widget">
                            <h3 class="title">Head Office</h3>
                            <ul class="contact-us">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <p>
                                        <strong class="contact-pad">Address:</strong>House #631, <br> Road 10, Avenue #3 <br> Mirpur DOHS<br>
                                        Dhaka, Bangladesh
                                    </p>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <p>
                                        <strong>Phone:</strong> +8802 51057312
                                    </p>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <p>
                                        <strong>Email:</strong><a title="email" href="javascript:void(0)">sales@bdfleet.com</a>
                                    </p>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /Contact Form -->
                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">

                        <div class="contact-info widget">
                            <h3 class="title">Business Hour</h3>
                            <ul class="business-hour">
                                <li><i class="fa fa-clock-o"> </i>Monday - Friday 9am to 5pm </li>
                                <li><i class="fa fa-clock-o"> </i>Saturday - 9am to 2pm</li>
                                <li><i class="fa fa-times-circle-o"> </i>Sunday - Closed</li>
                            </ul>
                        </div>

                    </div>
                    <!-- /Contact Form -->
                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">

                        <div class="follow widget">
                            <h3 class="title">Follow Us</h3>
                            <div class="member-social dark">
                                <a title="facebook" class="facebook" href="https://www.facebook.com/yumatechnology/" target="_blank"><i class="fa fa-facebook"></i></a>
                                
                                
                                
                                <a title="linkedin" class="linkedin" href="https://www.linkedin.com/company/bdfleet" target="_blank"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php get_footer(); ?>