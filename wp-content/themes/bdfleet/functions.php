<?php

require_once(get_template_directory().'/inc/enqueue.php');
require_once(get_template_directory().'/inc/navwalker.php');

register_nav_menus(array(
    'main_menu'    => 'Main Menu',
));